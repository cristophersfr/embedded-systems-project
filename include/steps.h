#ifndef STEPS_H
#define STEPS_H

#include <stdio.h>

#include <zephyr.h>
#include <misc/printk.h>
#include <board.h>
#include <gpio.h>
#include <device.h>
#include <sensor.h>
#include <irq.h>

#include <display/mb_display.h>

#include "accelerometer.h"
#include "compass.h"

void display_step();
void thermometer_step();
void accelerometer_step();
void compass_step();
void bluetooth_step();

#endif // STEPS_H
