#ifndef STATE_H
#define STATE_H

#include <stdio.h>

#include "steps.h"

#define NUM_STATES 5

// Each state needs to know its prev and next state, this will create a circular fsm.
typedef enum {
    STEP_1,
    STEP_2,
    STEP_3,
    STEP_4,
    STEP_5
} state_t;

typedef struct {
    void (*action)(void);
} mstate_t;

state_t state_machine_get_state();
void state_machine_set_state(state_t state);
void state_machine_next();
void state_machine_prev();
void state_machine_execute();

#endif // STATE_H
