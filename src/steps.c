#include "steps.h"
#include "display.h"

extern struct k_sem semaphore;

static struct device *temp_dev;

void display_step()
{
    struct mb_display *display = mb_display_get();

    mb_display_print(display, MB_DISPLAY_MODE_DEFAULT | MB_DISPLAY_FLAG_LOOP,
             K_MSEC(500), "ECOM042.2017.2");
}

void thermometer_step()
{
    int r;

    struct sensor_value temp_value;

    temp_dev = device_get_binding("TEMP_0");
    if (!temp_dev) {
        printf("error: no temp device\n");
    }

    r = sensor_sample_fetch(temp_dev);
    if (r) {
        printf("sensor_sample_fetch failed return: %d\n", r);
        return;
    }

    r = sensor_channel_get(temp_dev, SENSOR_CHAN_TEMP,
                   &temp_value);

    if (r) {
        printf("sensor_channel_get failed return: %d\n", r);
        return;
    }

    char temperature_string[32];

    float value = (float)temp_value.val1 + (float)temp_value.val2 / 1000000;

    sprintf(temperature_string, "%d.%dC", temp_value.val1, (int) temp_value.val2/100000);

    SYS_LOG_DBG("Temperature: %d.%d\n", temp_value.val1, (int) temp_value.val2/100000);

    struct mb_display *display = mb_display_get();

    mb_display_print(display, MB_DISPLAY_MODE_DEFAULT | MB_DISPLAY_FLAG_LOOP,
             K_MSEC(500), temperature_string);
}

static uint8_t point_x, point_y = 0;

void accelerometer_step()
{
    u8_t data[6];

    struct mb_display *display = mb_display_get();

    int16_t x = accelerometer_get_X(SIMPLE_CARTESIAN);
    int16_t y = accelerometer_get_Y(SIMPLE_CARTESIAN);

    SYS_LOG_DBG("\nACC: X: %d, Y: %d\n", x, y);

    // Horizontal scroll
    if(x <= -15) // MOVE LEFT
    {
        if(point_x > 0)
        {
            point_x--;
        }

    } else if(x >= 15) // MOVE RIGHT
    {
        if(point_x < 4){
            point_x++;
        }
    }

    // Vertical scroll
    if(y <= -15) // MOVE UP
    {
        if(point_y > 0)
        {
            point_y--;
        }

    } else if(y >= 15) // MOVE DOWN
    {
        if(point_y < 4){
            point_y++;
        }
    }

    display_reset();
    display_set_pixel(point_x, point_y);

    mb_display_image(display, MB_DISPLAY_MODE_SINGLE,
                     K_MSEC(250), display_image(), 1);

    k_sleep(K_MSEC(250));

    // Allow the main thread to continue the loop
    k_sem_give(&semaphore);
}

void compass_step()
{
    struct mb_display *display = mb_display_get();

    // Get the heading direction in degrees
    int heading = compass_heading();

    // Display offset
    int offset_x = 2;
    int offset_y = 2;

    SYS_LOG_DBG("\nCOMPASS: %d\n", heading);

    display_reset();
    for(int r = 0; r < 3; r++){
        display_set_pixel(floor(r*cos(heading) + offset_x), floor(r*sin(heading) + offset_y));
    }

    mb_display_image(display, MB_DISPLAY_MODE_SINGLE,
                     K_MSEC(250), display_image(), 1);

    k_sleep(K_MSEC(250));

    // Allow the main thread to continue the loop
    k_sem_give(&semaphore);
}

void bluetooth_step()
{
    printk("\nthis is state 5\n");
    return;
}
