/*! \brief Final project for Embedded Systems 2017.2 class.
 *
 *  This project consists of a state machine with 5
 *  different states, where each one execute a special task.
 *  These tasks are designed to put our knowledge discussed
 *  in class on practice. Working with MicroBit and its
 *  devices, such as display, bluetooth, accelerometer,
 *  magnetometer and thermometer.
 *
 *  @author: Cristopher Freitas
 *  @email: cristopherpk@gmail.com
 *  @date: May 27, 2018
 */

#include <zephyr.h>
#include <version.h>

#include "state.h"
#include "accelerometer.h"
#include "compass.h"

static s64_t button_a_timestamp;
static s64_t button_b_timestamp;

K_SEM_DEFINE(semaphore, 0, 1); /**< Semaphore value starts at 0, inserted to
manage context switching between states */


static void button_pressed(struct device *dev, struct gpio_callback *cb,
               u32_t pins) /** < This function handles button interruption*/
{
    if (pins & BIT(SW0_GPIO_PIN)) { // A IS PRESSED
        state_machine_prev();
        if (k_uptime_delta(&button_a_timestamp) < K_MSEC(100)) {
            printk("Too quick A presses\n");
            return;
        }
    } else { // B IS PRESSED
        state_machine_next();
        if (k_uptime_delta(&button_b_timestamp) < K_MSEC(100)) {
            printk("Too quick B presses\n");
            return;
        }
    }

    k_sem_give(&semaphore);
}

static void configure_buttons(void)
{
    static struct gpio_callback button_cb;
    struct device *gpio;

    gpio = device_get_binding(SW0_GPIO_NAME);

    gpio_pin_configure(gpio, SW0_GPIO_PIN,
               (GPIO_DIR_IN | GPIO_INT | GPIO_INT_EDGE |
                GPIO_INT_ACTIVE_LOW));
    gpio_pin_configure(gpio, SW1_GPIO_PIN,
               (GPIO_DIR_IN | GPIO_INT | GPIO_INT_EDGE |
                GPIO_INT_ACTIVE_LOW));
    gpio_init_callback(&button_cb, button_pressed,
               BIT(SW0_GPIO_PIN) | BIT(SW1_GPIO_PIN));
    gpio_add_callback(gpio, &button_cb);

    gpio_pin_enable_callback(gpio, SW0_GPIO_PIN);
    gpio_pin_enable_callback(gpio, SW1_GPIO_PIN);
}

int main()
{
    SYS_LOG_WRN("Firmware version: v%d.%d.%d",
                            VERSION_MAJOR, VERSION_MINOR, VERSION_BUILD);

    accelerometer_configure();

    compass_configure();

    configure_buttons();

    printk("\nWelcome to Microbit\n");

    while(1){
        state_machine_execute();
        k_sem_take(&semaphore, K_FOREVER);
    }

    return 0;
}
