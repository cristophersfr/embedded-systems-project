#include "display.h"

static struct mb_image image = MB_IMAGE(
{ 0, 0, 0, 0, 0 },
{ 0, 0, 0, 0, 0 },
{ 0, 0, 0, 0, 0 },
{ 0, 0, 0, 0, 0 },
{ 0, 0, 0, 0, 0 });

void display_set_pixel(uint8_t x, uint8_t y)
{
    image.row[y] = image.row[y] | BIT(x);
}

void display_get_pixel(uint8_t x, uint8_t y)
{
    return (image.row[y] & BIT(x)) >= 1;
}

void display_clear_pixel(uint8_t x, uint8_t y)
{
    image.row[y] = image.row[y] & ~BIT(x);
}

void display_reset()
{
    for(u8_t i = 0; i < 5; ++i)
        image.row[i] = 0;
}

struct mb_image * display_image(void){
    return &image;
}
